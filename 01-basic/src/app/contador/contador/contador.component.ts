import { Component } from "@angular/core";

@Component({
    selector: 'app-contador',
    template: `
        <h1>{{title}}</h1>
        <span>La base es: <b class="bold">{{base}}</b> </span>
        <br>
        <br>
        <button (click)="operation(+base)">+{{base}}</button>
        <span class="bold">{{contador}}</span>
        <button (click)="operation(-base)">-{{base}}</button>
        `
})
export class ContadorComponent {
  contador: number = 0;
  title: string = 'Contador App';
  numero: number = 10;
  base: number = 5;

  // aumentar = (valor: number) => {
  //   this.contador += valor;
  //   console.log( this.contador );
  // }  

  // restar = (valor: number) => {
  //   this.contador -= valor;
  //   console.log( this.contador );
  // }  

  operation = (valor: number) => this.contador += valor;
}
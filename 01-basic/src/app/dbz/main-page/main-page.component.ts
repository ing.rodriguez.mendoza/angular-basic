import { Component, Input } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

//[] : es para establecer un valor una propiedad a este objeto
//() : es para emitir o escuchar eventos

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent {

  //en angular esto se conoces como inyeccion de dependencia 
  //es parecido a la forma corta de definir propiedades en typescript
  //es importante mencionar que si en este componente se ha inicializado por primera vez ya no se vuelve a inicializar de nuevo en sintesis solo se inicializa una vez

  // personajes: Personaje[] = [];

  // constructor(private dbzService: DbzService){
  constructor(){
    // this.personajes = this.dbzService.personajes;
  }

  // cambiarNombre(event: any){
  //   console.log( event.target.value );
  // }

  nuevo : Personaje = {
    nombre: 'Lala',
    poder: 39000
  }

  // get personajes(): Personaje[] {
  //   return this.dbzService.personajes;
  // }
  

  // agregarNuevoPersonaje(personaje: Personaje) {
  //   console.log( 'llegamos' );
  //   console.log( personaje );
  //   this.personajes.push(personaje);
  // }

}

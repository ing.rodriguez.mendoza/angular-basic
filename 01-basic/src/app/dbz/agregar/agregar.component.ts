import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent {

  @Input() personajes: Personaje[] = [];

  @Input() nuevo: Personaje = {
    nombre: '',
    poder: 0
  }

  //emitir un valor al padre
  @Output() onNuevoPersonaje: EventEmitter<Personaje> = new EventEmitter();
  
  agregar(){
    // event?.preventDefault();//previene el procedimiento que tiene por defecto el submit de un formulario
    if (this.nuevo.nombre.trim().length === 0 || this.nuevo.poder === 0) {
      return;
    }
    // // console.log( event );
    // this.personajes.push(
    //   { nombre: this.nuevo.nombre, poder: this.nuevo.poder },
    // );
    console.log( 'this.nuevo', this.nuevo );
    //amterior
    // this.personajes.push( this.nuevo );
    //nuevo
    this.dbzService.agregarPersonaje(this.nuevo);
    // this.onNuevoPersonaje.emit(this.nuevo);
    // console.log( 'nombre', this.nuevo.nombre );
    // console.log( 'poder', this.nuevo.poder );

    // this.nuevo.nombre = '';
    // this.nuevo.poder = 0;
    this.nuevo = {
      nombre: '',
      poder: 0
    }
  }

  
  constructor(private dbzService: DbzService){

  }
  
}

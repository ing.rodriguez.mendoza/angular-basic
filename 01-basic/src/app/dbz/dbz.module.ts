import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';//manejo de formulario de angular el tradicional
import { MainPageComponent } from './main-page/main-page.component';
import { PersonajesComponent } from './personajes/personajes.component';
import { AgregarComponent } from './agregar/agregar.component';
import { DbzService } from './services/dbz.service';

//Siempre cuando creamos un nuevo componente en un modulo especifico debemos exportarlos
//en este caso creamos el componente MainPageComponent en automático se declara 
//pero debemos exportarlo manualmente
//Si queremos que componentes de este modulo se vean en el modulo principal 
//debemos importar el módulo DbzModule en el AppModule

@NgModule({
  declarations: [
    MainPageComponent,
    PersonajesComponent,
    AgregarComponent
  ],
  exports: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  //aqui ponemos todos los servicios que se van a utilizar estos van a servir como singleton es decir una unica instancia alrededor de todo ests modulo de dbz
  providers: [
    DbzService
  ]
})
export class DbzModule { }

import { Injectable } from "@angular/core";
import { Personaje } from "../interfaces/dbz.interface";


//un servicio es una clase abstracta donde se va centralizar la informacion y los metodos para interactuar con fuentes externas; para manipular el estado de informacion de la aplicacion
//en los servicios es donde se va a llamar los endpoints de los API Rest
//el tema de data y el manejo de la misma se debe centralizar en un servicio tenerlo en cuenta siempre
@Injectable()
export class DbzService{

    private _personajes: Personaje[] = [
        { nombre: 'Goku', poder:  30000 },
        { nombre: 'Maestro Rochi', poder: 16000 },
        { nombre: 'Bulma', poder: 17000 },
        { nombre: 'Vegeta', poder: 20555 },
    ];

    //javascript los objetos son enviados por referencia
    get personajes(): Personaje[]{
       //el operador spread(...)  premite una copia del objeto original en este caso de la propiedad de _personajes
       //utilizar dicho operador es una buena practica de javascript
       return [...this._personajes];
    }

    agregarPersonaje(personaje: Personaje)
    {
        // let obPersonaje = [...this._personajes];
        // obPersonaje.push(personaje);
        this._personajes.push(personaje);
    }
    
    constructor(){
        // console.log( 'hola desde el DbzService' );
    }
}
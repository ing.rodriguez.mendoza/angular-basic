import { Component } from '@angular/core';

@Component({
  selector: 'app-listado2',
  templateUrl: './listado2.component.html',
  styleUrls: ['./listado2.component.css']
})
export class Listado2Component {

  heroes: string[] = ['Spiderman','Hulk','Superman','Batman'];
  heroe: string = '';
  heroeBorrado: boolean= false;

  eliminar() {
    this.heroe = this.heroes.shift() || '';
    // this.heroe = this.heroes.splice(0,1);
    // console.log( 'this.heroes.length', this.heroes.length );
    // this.heroe !== ''? this.heroeBorrado = true : this.heroeBorrado = false;
    // this.heroes.length === 0?  this.heroe='No existe heroes por eliminar':  this.heroe=  this.heroe;
    // console.log( 'Heroe eliminado', this.heroe );
  }

}

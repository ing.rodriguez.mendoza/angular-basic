import { Component } from '@angular/core';

//Cuando un compopnente este siempre tiene que estar importado en algún módulo
//Una buena practica a seguir es que debemos agrupar nuestra aplicación en módulos
@Component({
    selector: 'app-heroe',
    templateUrl: 'heroe.component.html',
})
export class HeroeComponent {
    nombre: string = 'Iroman';
    edad: number = 20;

    obtenerNombre = (): string => `${this.nombre} - ${this.edad}`

    //como el componente es una clase podemos utilizar get / set
    get NombreCapitalizado(): string {
        return this.nombre.toUpperCase();
    }

    cambiarNombre() {
        this.nombre = 'Spiderman';
        // console.log( 'holaaa' );
    }

    cambiarEdad = (): void =>
    { 
        this.edad = 50;
        // console.log( 'chauuu' );
    }
}
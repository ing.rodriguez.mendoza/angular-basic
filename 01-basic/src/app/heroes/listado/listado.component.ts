// import { Component, OnInit } from '@angular/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
})
// export class ListadoComponent implements OnInit {
export class ListadoComponent {

  // constructor() { 
  //   console.log( 'soy el constructor' );
  // }

  // ngOnInit(): void {
  //   console.log( 'soy la implementacion de OnInit' );
  // }

  heroes: string[] = ['Capitana Marvel','Hulk','La Viuda Negra'];

  borrarHeroe = (index: number) => this.heroes.splice(index,1);

}
